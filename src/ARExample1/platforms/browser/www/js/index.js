/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');

        window.or = {
          x: 0,
          y: 0,
          z: 0
        };

        window.gps = {
          x: 0,
          y: 0,
          z: 0
        };

        window.addEventListener('deviceorientation', function(e) {
          window.or.x = e.gamma;
          window.or.y = e.beta;
          window.or.z = e.alpha;
        });

        window.setInterval(function () {
          console.log("fetching location data...");
          navigator.geolocation.getCurrentPosition(function (o) {
            window.gps.x = o.coords.latitude;
            window.gps.y = o.coords.longitude;
            window.gps.z = o.coords.altitude;
          }, function () {
            console.log("geolocation failed.");
          });
        }, 1000);

        document._dbg = function (o, _d) {
          if (typeof(_d) == typeof(undefined)) {
            _d = 0;
          }

          if (_d > 10) return "max recursion depth reached";

          if (typeof(o) == "object") {
            var msg = "{";


            for (var i in o) {
              msg += i.toString() + ": " + document._dbg(o[i], _d + 1) + ", ";
            }

            return msg.substring(0, msg.length - 2) + "}";

          } else if (typeof(o) == "function") {
            return o.toString();
          } else {
            return o || "null";
          }
        }

        document.dbg = function (o) {
          var out = $(".output#output");

          var msg = document._dbg(o);

          out.append($("<div></div>").text(msg).attr("class", "line"));
        }

        window.nextTab = function () {
          var active = $(".tab.active");
          var next;

          if ((next = $(".tab[tabindex=" + (+active.attr("tabindex") + 1) + "]")).get().length > 0) {
            active.attr("class", active.attr("class").replace("active", "").trim());
            next.attr("class", active.attr("class").trim() + " active");
          }

          document.dbg("swipeleft");
        };

        window.prevTab = function () {
          var active = $(".tab.active");
          var next;

          // get tab max? shitty method

          if ((next = $(".tab[tabindex=" + (+active.attr("tabindex") - 1) + "]")).get().length > 0) {
            active.attr("class", active.attr("class").replace("active", "").trim());
            next.attr("class", active.attr("class").trim() + " active");
          }

          document.dbg("swiperight");
        };

        $(window).on("swipeleft", window.nextTab);

        $(window).on("swiperight", window.prevTab);

        $(".next").on("click", window.nextTab);
        $(".prev").on("click", window.prevTab);

        console.log($(".next"));

        console.log("console.log will be overridden.");

        console._log = console.log;
        console.log = document.dbg;

        console.log("console.log has been overridden");

        ezar.initializeVideoOverlay(
          function(){
            ezar.getBackCamera().start(function () {
              console.log("camera started.");
              document.body.style.backgroundColor = "transparent";
            }, function () {
              console.log("failed starting camera");
            });
          }
        )
        window.setInterval(function () {
          $("#anglex").text(window.or.x);
          $("#angley").text(window.or.y);
          $("#anglez").text(window.or.z);

          $("#posx").text(window.gps.x);
          $("#posy").text(window.gps.y);
          $("#posz").text(window.gps.z);
        }, 500);
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {

    }
};



app.initialize();
